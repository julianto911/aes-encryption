package main

import (
	"bytes"
	"crypto/aes"
	"encoding/base64"
	"errors"
	"fmt"
	"time"
)

const (
	preSharedKey = "zWM68zYXPRQVPrti"
	preSharedID  = "4620"
)

func makeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func getKey(data string) (string, error) {
	handleErr := func(err error) (string, error) {
		return "", err
	}

	//1.generate key
	key := []byte(preSharedKey)

	//2.encrypt aes
	val, err := encryptAES(data, key)
	if err != nil {
		return handleErr(err)
	}

	return val, nil
}

func encryptAES(data string, key []byte) (string, error) {
	errNotFullBlock := errors.New("input not full blocks")
	errIODifferentLength := errors.New("I/O different length")
	handleErr := func(err error) (string, error) {
		return "", fmt.Errorf("AES encryption process > %w", err)
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return handleErr(err)
	}

	mode := NewECBEncrypter(block)
	content := pkcs5Padding([]byte(data), block.BlockSize())
	crypted := make([]byte, len(content))

	if len(crypted)%mode.BlockSize() != 0 {
		return handleErr(errNotFullBlock)
	}
	if len(crypted) < len(content) {
		return handleErr(errIODifferentLength)
	}

	mode.CryptBlocks(crypted, content)

	return base64.StdEncoding.EncodeToString(crypted), nil
}

func pkcs5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}
