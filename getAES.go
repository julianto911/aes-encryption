package main

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
)

func getAES(data string) (string, error) {
	key := []byte(preSharedKey)

	plaintext := []byte(data)
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	nonce := make([]byte, aesGCM.NonceSize())
	ciphertext := aesGCM.Seal(nonce, nonce, plaintext, nil)

	return base64.StdEncoding.EncodeToString(ciphertext), nil
}
