package main

import (
	"fmt"
	"strconv"
)

func main() {
	data := preSharedID + "#" + strconv.FormatInt(makeTimestamp(), 10)
	fmt.Println("raw val:", data)
	encrypted, err := getAES(data)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("use getAES result :", encrypted)

	encrypted, err = getKey(data)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("use getKey result :", encrypted)
}
